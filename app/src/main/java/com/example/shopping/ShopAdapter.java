package com.example.shopping;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

public class ShopAdapter extends RecyclerView.Adapter<ShopViewHolder> implements Filterable {

    ArrayList<ListItem> shopData, shopDataFull;
    Context c;

    public ShopAdapter(Context c, ArrayList<ListItem> shopData) {
        this.c = c;
        this.shopData = shopData;
        shopDataFull = new ArrayList<>(shopData);
    }

    @NonNull
    @Override
    public ShopViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(c).inflate(R.layout.shop_row_layout, parent, false);
        ShopViewHolder Vi = new ShopViewHolder(v);
        return Vi;
    }

    @Override
    public void onBindViewHolder(@NonNull final ShopViewHolder holder, int position) {
        final ListItem listItem = shopData.get(position);
        Picasso.get().load(listItem.getImageUrl()).into(holder.shopImage);
        holder.shopName.setText(listItem.getName());
        holder.shopLocation.setText(listItem.getLocation());
    }

    @Override
    public int getItemCount() {
        return shopData.size();
    }

    public Filter getFilter() {
        return shopFilter;
    }

    private  Filter shopFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<ListItem> filterList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filterList.addAll(shopDataFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (ListItem item: shopDataFull) {
                    if(item.getName().toLowerCase().contains(filterPattern)) {
                        filterList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filterList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            shopData.clear();
            shopData.addAll((List)results.values);
            notifyDataSetChanged();
        }
    };
}
