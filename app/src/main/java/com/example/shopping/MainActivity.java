package com.example.shopping;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.SearchView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static androidx.recyclerview.widget.LinearLayoutManager.VERTICAL;

public class MainActivity extends AppCompatActivity {
    ArrayList<ListItem> shopData = new ArrayList<ListItem>();
    RecyclerView RV;
    ShopAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getData();
        setUpRecyclerView();
    }

    private void getData() {
        shopData.add(new ListItem("https://picsum.photos/id/237/200/300","General Store","Adabor, Dhaka"));
        shopData.add(new ListItem("https://picsum.photos/id/237/200/300","Meena Bazar","Adabor, Dhaka"));
        shopData.add(new ListItem("https://picsum.photos/id/237/200/300","Karim Store","Adabor, Dhaka"));
        shopData.add(new ListItem("https://picsum.photos/id/237/200/300","Rahim Store","Adabor, Dhaka"));
        shopData.add(new ListItem("https://picsum.photos/id/237/200/300","Express Store","Adabor, Dhaka"));
    }

    private void setUpRecyclerView() {
        RV = findViewById(R.id.recyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(VERTICAL);
        RV.setLayoutManager(llm);

        adapter = new ShopAdapter(MainActivity.this, shopData);
        RV.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }
}