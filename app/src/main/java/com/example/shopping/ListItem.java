package com.example.shopping;

public class ListItem {
    private String imageUrl;
    private String name;
    private String location;

    public ListItem(String imageUrl, String name, String location) {
        this.imageUrl = imageUrl;
        this.name = name;
        this.location = location;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }
}
