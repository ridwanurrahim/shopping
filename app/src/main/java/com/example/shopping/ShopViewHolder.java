package com.example.shopping;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ShopViewHolder extends RecyclerView.ViewHolder {
    ImageView shopImage;
    TextView shopName, shopLocation;

    public ShopViewHolder(@NonNull View itemView) {
        super(itemView);
        shopImage = (ImageView) itemView.findViewById(R.id.shopImage);
        shopName = (TextView) itemView.findViewById(R.id.shopName);
        shopLocation = (TextView) itemView.findViewById(R.id.shopLocation);

    }
}
